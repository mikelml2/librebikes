const ProductSelection = ({ addToCart }) => {
  return (
    <section id="product-selection">
      <div className="title">
        <h1 className="text">Nuestro Catálogo</h1>
        <div className="line"></div>
      </div>
      <div className="content">
        <h1 className="title">Bicicletas</h1>
        <div className="line"></div>
        <div className="category bikes" id="mountain-bicycles">
          <h1 className="title">Montaña</h1>
          <div className="card">
            <img src="/img/product-selection/m1.jpg" className="image" />
            <div className="details">
              <h1 className="title">Corratec ZZYZX</h1>
              <p className="price">$15000.00</p>
              <p className="description">Versátil, ágil y ligera. </p>
              <button className="action" onClick={() => addToCart('Corratec ZZYZX', 15000, 'm1')}>Agregar al carrito</button>
            </div>
          </div>
          <div className="card">
            <img src="/img/product-selection/m2.jpg" className="image" />
            <div className="details">
              <h1 className="title">Corratec XVERT</h1>
              <p className="price">$20000.00</p>
              <p className="description">Potente, veloz y durable.</p>
              <button className="action" onClick={() => addToCart('Corratec XVERT', 20000, 'm2')}>Agregar al carrito</button>
            </div>
          </div>
        </div>
        <div className="category bikes" id="cruiser-bicycles">
          <h1 className="title">Cruiser</h1>
          <div className="card">
            <img src="/img/product-selection/c1.jpg" className="image" />
            <div className="details">
              <h1 className="title">Trek Cruiser</h1>
              <p className="price">$11000.00</p>
              <p className="description">Cómoda, simple y bella.</p>
              <button className="action" onClick={() => addToCart('Trek Cruiser', 11000, 'c1')}>Agregar al carrito</button>
            </div>
          </div>
          <div className="card">
            <img src="/img/product-selection/c2.jpg" className="image" />
            <div className="details">
              <h1 className="title">Urban Delight</h1>
              <p className="price">$8000.00</p>
              <p className="description">Tradicional, ergonómica y práctica.</p>
              <button className="action" onClick={() => addToCart('Urban Delight', 8000, 'c2')}>Agregar al carrito</button>
            </div>
          </div>
        </div>
        <h1 className="title">Accesorios</h1>
        <div className="line"></div>
        <div className="category helmets" id="helmets">
          <h1 className="title">Cascos</h1>
          <div className="card">
            <img src="/img/product-selection/h1.jpg" className="image" />
            <div className="details">
              <h1 className="title">Casco Garneau</h1>
              <p className="price discount">$900.00</p>
              <p className="price">$765.00</p>
              <p className="description">Para carreras.</p>
              <button className="action" onClick={() => addToCart('Casco Garneau', 765, 'h1')}>Agregar al carrito</button>
            </div>
          </div>
          <div className="card">
            <img src="/img/product-selection/h2.jpg" className="image" />
            <div className="details">
              <h1 className="title">Casco Scott</h1>
              <p className="price discount">$700.00</p>
              <p className="price">$560.00</p>
              <p className="description">Para uso cotidiano.</p>
              <button className="action" onClick={() => addToCart('Casco Scott', 560, 'h2')}>Agregar al carrito</button>
            </div>
          </div>
          <div className="card">
            <img src="/img/product-selection/h3.jpg" className="image" />
            <div className="details">
              <h1 className="title">Casco Kross</h1>
              <p className="price discount">$1100.00</p>
              <p className="price">$990.00</p>
              <p className="description">Para velocistas.</p>
              <button className="action" onClick={() => addToCart('Casco Kross', 990, 'h3')}>Agregar al carrito</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductSelection
