const ContactUs = () => {
  return (
    <section id="contact-us">
      <h1 className="title">¡Comunicate con Nosotros!</h1>
      <div className="column">
        <i className="material-icons icon">phone</i>
        <br/>
        <a href="tel:+526188290900" target="_blank" className="link">618 829 0900</a>
      </div>
      <div className="column">
        <i className="material-icons icon">mail</i>
        <br/>
        <a href="mailto:ventas@librebikes.septum.io" target="_blank" className="link">ventas@librebikes.septum.io</a>
      </div>
      <div className="column">
        <i className="material-icons icon">language</i>
        <br/>
        <a href="https://facebook.com" target="_blank" className="link">LIBREBIKES Facebook</a>
      </div>
    </section>
  )
}

export default ContactUs
