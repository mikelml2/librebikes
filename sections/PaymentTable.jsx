const PaymentTable = () => {
  const [openModal, setOpenModal] = React.useState(false)
  const [numberItems, setNumberItems] = React.useState(0)
  const [itemsArray, setItemsArray] = React.useState([])

  React.useEffect(() => {
    try {
      const itemsJson = window.localStorage.getItem('shoppingCartItems')

      if (itemsJson) {
        const itemsArray = JSON.parse(itemsJson)

        if (itemsArray.length) {
          setItemsArray(itemsArray)
          setNumberItems(itemsArray.length)
        } else {
          setOpenModal(false)
          setNumberItems(0)
          window.history.back()
        }
      } else {
        setOpenModal(false)
        setNumberItems(0)
        window.history.back()
      }
    } catch {
      setOpenModal(false)
      setNumberItems(0)
      window.history.back()
    }
  }, [])

  const handleRemoveItem = index => {
    try {
      const itemsArrayCopy = [...itemsArray]

      if (!(itemsArrayCopy.length - 1)) {
        setOpenModal(false)
        setNumberItems(0)
        return window.history.back()
      }

      itemsArrayCopy.splice(index, 1)
      const itemsArrayCopyJson = JSON.stringify(itemsArrayCopy)
      window.localStorage.setItem('shoppingCartItems', itemsArrayCopyJson)
      setItemsArray(itemsArrayCopy)
      setNumberItems(numberItems => numberItems - 1)
    } catch { }
  }

  const handleItemsPayment = () => {
    setOpenModal(true)
    window.localStorage.setItem('shoppingCartItems', '[]')
    setItemsArray([])
    setNumberItems(0)
  }

  return (
    <section id="payment-table">
      <div className="title">
        <h1 className="text">Pago de Productos</h1>
        <div className="line"></div>
      </div>
      {numberItems ? (
        <div className="table">
          <div className="row header">
            <span className="column">#</span>
            <span className="column">Imagen</span>
            <span className="column">Producto</span>
            <span className="column">Precio</span>
            <span className="column"></span>
          </div>
          {
            itemsArray.map(({ item, price, image }, index) => (
              <div className="row" key={index}>
                <span className="column">{index + 1}</span>
                <span className="column">
                  <img src={`/img/payment-table/${image}.jpg`} className="image" />
                </span>
                <span className="column">{item}</span>
                <span className="column">${price}.00</span>
                <span className="column">
                  <button className="action" onClick={() => handleRemoveItem(index)}>
                    <i className="material-icons icon">close</i>
                  </button>
                </span>
              </div>
            ))
          }
          {numberItems ? (
            <div className="row footer">
              <span className="column">{itemsArray.length}</span>
              <span className="column"></span>
              <span className="column">{}</span>
              <span className="column">${itemsArray.reduce((total, { price }) => total + price, 0)}.00</span>
              <span className="column"></span>
            </div>
          ) : null}
        </div>
      ) : null}
      {numberItems ? (
        <div className="controls">
          <button className="action" onClick={handleItemsPayment}>PAGAR</button>
        </div>
      ) : null}
      {openModal ? (
        <div className="overlay">
          <div className="wrapper">
            <div className="modal">
              <div className="controls">
                <button className="action" onClick={() => { setOpenModal(false); window.history.back(); }}>
                  <i className="material-icons icon">
                    close
                  </i>
                </button>
              </div>
              <div className="title">
                <h1 className="text">¡Gracias por tu preferencia!</h1>
                <div className="line"></div>
              </div>
              <div className="content">
                <p className="notice">Tu pedido llegara de 3 a 5 días hábiles.</p>
              </div>
              <div className="footer">
                <a href="/products" className="action">Volver a los productos</a>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </section>
  )
}

export default PaymentTable
