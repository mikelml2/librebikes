const CallToAction = () => {
  return (
    <section id="call-to-action">
      <h1 className="phrase">¡Obtén las mejores ofertas en tu correo!</h1>
      <div className="form">
        <input className="field" type="text" placeholder="tu@correo.com" />
        <button className="action">
          Enviar
        </button>
      </div>
    </section>
  )
}

export default CallToAction
