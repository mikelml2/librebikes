const Categories = () => {
  return (
    <section id="categories">
      <div className="title">
        <h1 className="text">Nuestras Bicicletas</h1>
        <div className="line"></div>
      </div>
      <div className="products">
        <a className="card" href="/products#mountain-bicycles">
          <img src="/img/categories/mountain.jpg" className="image"/>
          Montaña
        </a>
        <a className="card" href="/products#cruiser-bicycles">
          <img src="/img/categories/cruiser.jpg" className="image"/>
          Cruiser
        </a>
      </div>
    </section>
  )
}

export default Categories
