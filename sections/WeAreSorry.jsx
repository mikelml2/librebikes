const WeAreSorry = () => {
  return (
    <section id="we-are-sorry">
      <div className="title">
        <h1 className="text">¡Lo sentimos!</h1>
        <div className="line"></div>
      </div>
      <div className="content">
        <div className="column">
          <p className="description">
            La pagina que intenta acceder no existe. Te recomendamos hacer esto:
          </p>
          <ul className="list">
            <li className="item">
              Regresa a la pagina principal dando clic a <a href="/" className="link">este enlace</a>.
            </li>
          </ul>
        </div>
        <div className="column">
          <img src="/img/we-are-sorry/photo.jpg" className="photo"/>
        </div>
      </div>
    </section>
  )
}

export default WeAreSorry
