const ShoppingCart = ({ externalNumber }) => {
  const [openModal, setOpenModal] = React.useState(false)
  const [numberItems, setNumberItems] = React.useState(0)
  const [itemsArray, setItemsArray] = React.useState([])

  React.useEffect(() => {
    try {
      const itemsJson = window.localStorage.getItem('shoppingCartItems')

      if (itemsJson) {
        const itemsArray = JSON.parse(itemsJson)

        setItemsArray(itemsArray)
        setNumberItems(itemsArray.length)
      } else {
        setOpenModal(false)
        setNumberItems(0)
      }
    } catch {
      setOpenModal(false)
      setNumberItems(0)
    }
  }, [externalNumber])

  const handleRemoveItem = index => {
    try {
      const itemsArrayCopy = [...itemsArray]
      itemsArrayCopy.splice(index, 1)
      const itemsArrayCopyJson = JSON.stringify(itemsArrayCopy)
      window.localStorage.setItem('shoppingCartItems', itemsArrayCopyJson)
      setItemsArray(itemsArrayCopy)
      setNumberItems(numberItems => numberItems - 1)
    } catch { }
  }

  return (
    <section id="shopping-cart">
      <div className="controls">
        {numberItems ? (
          <button className="action" onClick={() => setOpenModal(true)}>
            <i className="material-icons icon">
              shopping_cart
            </i>
            <div className="items">{numberItems}</div>
          </button>
        ) : null}
      </div>
      {numberItems && openModal ? (
        <div className="overlay">
          <div className="wrapper">
            <div className="modal">
              <div className="controls">
                <button className="action" onClick={() => setOpenModal(false)}>
                  <i className="material-icons icon">
                    close
                  </i>
                </button>
              </div>
              <div className="title">
                <h1 className="text">Productos</h1>
                <div className="line"></div>
              </div>
              {numberItems ? (
                <div className="table">
                  <div className="row header">
                    <span className="column">Producto</span>
                    <span className="column">Precio</span>
                    <span className="column"></span>
                  </div>
                  {
                    itemsArray.map(({ item, price }, index) => (
                      <div className="row" key={index}>
                        <span className="column">{item}</span>
                        <span className="column">${price}.00</span>
                        <span className="column">
                          <button className="action" onClick={() => handleRemoveItem(index)}>
                            <i className="material-icons icon">close</i>
                          </button>
                        </span>
                      </div>
                    ))
                  }
                </div>
              ) : null}
              <div className="footer">
                <a href="/payment" className="action">Proceder al pago</a>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </section>
  )
}

export default ShoppingCart
