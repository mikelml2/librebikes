const AboutUs = () => {
  return (
    <section id="about-us">
      <div className="title">
        <h1 className="text">Sobre Nosotros</h1>
        <div className="line"></div>
      </div>
      <div className="content">
        <p className="text">
          LIBREBIKES es una empresa orgullosamente Duranguense. Fundada en 2019 con el
          nombre de DCU WEBSITE, el 6 de Diciembre de 2019 cambia su nombre a LIBREBIKES
          y cuya naturaleza es la comercialización de bicicletas y accesorios.
        </p>
        <a href="/about-us" className="action">
          Saber Más
          <i className="icon material-icons">
            arrow_right_alt
          </i>
        </a>
      </div>
    </section>
  )
}

export default AboutUs
