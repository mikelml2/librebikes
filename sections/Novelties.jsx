const Novelties = () => {
  return (
    <section id="novelties">
      <div className="column photo" />
      <div className="column text">
        <h1 className="phrase">Novedades</h1>
        <a href="/products#product-selection" className="action">
          Explorar
          <i className="icon material-icons">
            arrow_right_alt
          </i>
        </a>
      </div>
      <div className="column photo" />
    </section>
  )
}

export default Novelties
