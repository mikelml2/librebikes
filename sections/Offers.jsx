const Offers = () => {
  return (
    <section id="offers">
      <div className="title">
        <h1 className="text">Ofertas</h1>
        <div className="line"></div>
      </div>
      <div className="products">
        <a className="card" href="/products#helmets">
          <img src="/img/offers/helmet-1.jpg" className="image" />
          Casco Garneau <br/>
          15% de descuento
        </a>
        <a className="card" href="/products#helmets">
          <img src="/img/offers/helmet-2.jpg" className="image" />
          Casco Scott <br/>
          20% de descuento
        </a>
        <a className="card" href="/products#helmets">
          <img src="/img/offers/helmet-3.jpg" className="image" />
          Casco Kross <br/>
          10% de descuento
        </a>
      </div>
    </section>
  )
}

export default Offers
