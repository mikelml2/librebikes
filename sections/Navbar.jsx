const Navbar = ({ activeLink }) => {
  return (
    <section id="navbar">
      <nav className="nav">
        <div className="logotype">
          <a className="text" href="/">
            LIBRE
            <span className="accent">
              BIKES
            </span>
          </a>
        </div>
        <div className="links">
          <a className={'link' + (activeLink === '/' ? ' active' : '')} href="/">Inicio</a>
          <a className={'link' + (activeLink === '/products' ? ' active' : '')} href="/products">Productos</a>
          <a className={'link' + (activeLink === '/about-us' ? ' active' : '')} href="/about-us">Nosotros</a>
          <a className={'link' + (activeLink === '/contact' ? ' active' : '')} href="/contact">Contacto</a>
        </div>
      </nav>
    </section>
  )
}

export default Navbar
