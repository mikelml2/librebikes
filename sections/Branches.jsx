const Branches = () => {
  return (
    <section id="branches">
      <div className="title">
        <h1 className="text">Sucursales</h1>
        <div className="line"></div>
      </div>
      <div className="map">
        <div className="here">
          <h1 className="title">¡Aqui Estamos!</h1>
          <p className="description">
            LIBREBIKES cuenta con una sucursal en un estado de la República Mexicana:
          </p>
          <ul className="list">
            <li className="item">Durango - Blvd. Felipe Pescador 1830, Nueva Vizcaya, 34080</li>
          </ul>
        </div>
        <iframe
          className="google"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3643.96119344897!2d-104.64863858549248!3d24.032433583861497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869bb7c4a8a1b419%3A0x540db05b84d37014!2sInstituto%20Tecnol%C3%B3gico%20De%20Durango!5e0!3m2!1sen!2smx!4v1575855179819!5m2!1sen!2smx"
          frameBorder="0"
          style={{border: 0}}
          allowFullScreen=""
        />
      </div>
    </section>
  )
}

export default Branches
