const HeaderCard = ({ title }) => {
  return (
    <section id="header-card">
      <div className="title-box">
        <h1 className="title">{title}</h1>
        <div className="line"></div>
      </div>
    </section>
  )
}

export default HeaderCard
