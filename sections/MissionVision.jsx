const MissionVision = () => {
  return (
    <section id="mission-vision">
      <div className="title">
        <h1 className="text">Misión y Visión</h1>
        <div className="line"></div>
      </div>
      <div className="content">
        <div className="column">
          <h1 className="title">Misión</h1>
          <p className="description">
            Brindar un medio de transporte al alcance de la población. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae ullam esse voluptatibus enim qui mollitia distinctio nobis vero quaerat amet illum, eius dolor incidunt magni architecto magnam soluta quia accusamus.
          </p>
          <h1 className="title">Visión</h1>
          <p className="description">
            Ser una empresa líder de nivel internacional. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus iusto distinctio ipsum doloremque, sit error molestias quis, iure aliquid, in voluptas quasi hic. Laboriosam dolore maxime dolorum vero, vitae dignissimos.
          </p>
        </div>
        <div className="column">
          <img src="/img/mission-vision/photo.jpg" className="photo"/>
        </div>
      </div>
    </section>
  )
}

export default MissionVision
