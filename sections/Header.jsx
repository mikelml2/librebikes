const Header = () => {
  return (
    <section id="header">
      <div className="overlay">
        <div className="box">
          <h1 className="phrase">Busca Tu Libertad</h1>
          <a href="/products" className="action">Comprar Ahora</a>
        </div>
        <div className="controls">
          <a href="#categories" className="anchor">
            <i className="material-icons icon">
              keyboard_arrow_down
            </i>
          </a>
        </div>
      </div>
    </section>
  )
}

export default Header
