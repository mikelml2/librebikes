const Footer = () => {
  return (
    <section id="footer">
      <p className="notice">LIBREBIKES &copy;2019</p>
      <p className="disclaimer">
        Este sitio no es una página web comercial real, es un ejercicio de práctica de la materia de Diseño Centrado en el Usuario. Departamento de Sistemas y Computación. Instituto Tecnológico de Durango, México. Los nombres de marcas y fotografías son propiedad de sus creadores originales y sólo están siendo usados con fines académicos. Diciembre 2019.
      </p>
    </section>
  )
}

export default Footer
