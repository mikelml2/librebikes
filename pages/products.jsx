import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import HeaderCard from '../sections/HeaderCard.jsx'
import ProductSelection from '../sections/ProductSelection.jsx'
import CallToAction from '../sections/CallToAction.jsx'
import Footer from '../sections/Footer.jsx'
import ShoppingCart from '../sections/ShoppingCart.jsx'

const Products = () => {
  const [externalNumber, setExternalNumber] = React.useState(0)

  const handleAddToCart = (item, price, image) => {
    try {
      const itemsJson = window.localStorage.getItem('shoppingCartItems')
      if (itemsJson) {
        const itemsArray = JSON.parse(itemsJson)
        const newItemsArray = [...itemsArray, { item, price, image }]
        const newItemsArrayJson = JSON.stringify(newItemsArray)

        window.localStorage.setItem('shoppingCartItems', newItemsArrayJson)
        setExternalNumber(externalNumber => externalNumber + 1)
      } else {
        const itemsArray = [{ item, price, image }]
        const itemsArrayJson = JSON.stringify(itemsArray)

        window.localStorage.setItem('shoppingCartItems', itemsArrayJson)
        setExternalNumber(externalNumber => externalNumber + 1)
      }
    } catch { }
  }

  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — Productos</title>
      </Head>
      <Navbar activeLink="/products" />
      <HeaderCard title="Productos" />
      <ProductSelection addToCart={handleAddToCart} />
      <CallToAction />
      <Footer />
      <ShoppingCart externalNumber={externalNumber} />
    </React.Fragment>
  )
}

export default Products
