import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import Header from '../sections/Header.jsx'
import Categories from '../sections/Categories.jsx'
import Novelties from '../sections/Novelties.jsx'
import Offers from '../sections/Offers.jsx'
import AboutUs from '../sections/AboutUs.jsx'
import CallToAction from '../sections/CallToAction.jsx'
import Footer from '../sections/Footer.jsx'
import ShoppingCart from '../sections/ShoppingCart.jsx'

const Index = () => {
  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — Busca tu Libertad</title>
      </Head>
      <Navbar activeLink="/" />
      <Header />
      <Categories />
      <Novelties />
      <Offers />
      <AboutUs />
      <CallToAction />
      <Footer />
      <ShoppingCart />
    </React.Fragment>
  )
}

export default Index
