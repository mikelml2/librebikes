import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import HeaderCard from '../sections/HeaderCard.jsx'
import WeAreSorry from '../sections/WeAreSorry.jsx'
import Footer from '../sections/Footer.jsx'

const ErrorPage = () => {
  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — 404</title>
      </Head>
      <Navbar activeLink="/" />
      <HeaderCard title="404" />
      <WeAreSorry />
      <Footer />
    </React.Fragment>
  )
}

export default ErrorPage
