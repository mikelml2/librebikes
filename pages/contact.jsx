import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import HeaderCard from '../sections/HeaderCard.jsx'
import Branches from '../sections/Branches.jsx'
import ContactUs from '../sections/ContactUs.jsx'
import CallToAction from '../sections/CallToAction.jsx'
import Footer from '../sections/Footer.jsx'
import ShoppingCart from '../sections/ShoppingCart.jsx'

const Contact = () => {
  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — Contacto</title>
      </Head>
      <Navbar activeLink="/contact" />
      <HeaderCard title="Contacto" />
      <Branches />
      <ContactUs />
      <CallToAction />
      <Footer />
      <ShoppingCart />
    </React.Fragment>
  )
}

export default Contact
