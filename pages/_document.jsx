import Document, { Html, Head, Main, NextScript } from 'next/document'

class NextDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta property="og:title" content="LIBREBIKES — Busca tu Libertad"/>
          <meta property="og:url" content="https://librebikes.septum.io"/>
          <meta property="og:site_name" content="LIBREBIKES"/>
          <meta property="og:image" content="https://librebikes.septum.io/img/og/image.jpg"/>
          <meta property="og:type" content="website"/>
          <meta property="og:description" content="Busca tu Libertad. Este sitio no es una página web comercial real, es un ejercicio de práctica de la materia de Diseño Centrado en el Usuario. Departamento de Sistemas y Computación. Instituto Tecnológico de Durango, México. Los nombres de marcas y fotografías son propiedad de sus creadores originales y sólo están siendo usados con fines académicos. Diciembre 2019."/>
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
          <link rel="manifest" href="/site.webmanifest" />
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#e57712" />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#e57712" />
          <link rel="stylesheet" href="/css/main.css" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default NextDocument
