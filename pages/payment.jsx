import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import HeaderCard from '../sections/HeaderCard.jsx'
import PaymentTable from '../sections/PaymentTable.jsx'
import Offers from '../sections/Offers.jsx'
import ContactUs from '../sections/ContactUs.jsx'
import CallToAction from '../sections/CallToAction.jsx'
import Footer from '../sections/Footer.jsx'

const Payment = () => {
  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — Pago de Productos</title>
      </Head>
      <Navbar activeLink="/products" />
      <HeaderCard title="Pago" />
      <PaymentTable />
      <Offers />
      <ContactUs />
      <CallToAction />
      <Footer />
    </React.Fragment>
  )
}

export default Payment
