import Head from 'next/head'

import Navbar from '../sections/Navbar.jsx'
import HeaderCard from '../sections/HeaderCard.jsx'
import MissionVision from '../sections/MissionVision.jsx'
import CallToAction from '../sections/CallToAction.jsx'
import Footer from '../sections/Footer.jsx'
import ShoppingCart from '../sections/ShoppingCart.jsx'

const AboutUs = () => {
  return (
    <React.Fragment>
      <Head>
        <title>LIBREBIKES — Nosotros</title>
      </Head>
      <Navbar activeLink="/about-us" />
      <HeaderCard title="Nosotros" />
      <MissionVision />
      <CallToAction />
      <Footer />
      <ShoppingCart />
    </React.Fragment>
  )
}

export default AboutUs
